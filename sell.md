---
layout: sell
---

Got records collecting dust in the attic? Bring in your collection of LPs, CDs, or DVDs! After a brief assessment of quality, we can offer you CASH for your old media, or in-store credit if something new catches your eye...

---

Can't make it into the store? No worries! We can travel for collections of the right size and quality.

---

Give us a call or send us an email with the subject line "Looking to sell" briefly describing your stock and we'll be happy to message you back! Walk-ins are welcomed as well.