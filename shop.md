---
# Frequently Asked Questions
layout: shop
---

### What is your return policy?

If you are ever unhappy with the quality of the items you purchased, please bring the product and your receipt back to the store within 10 days and we will offer you in-store credit. **NO CASH REFUNDS.** 

**Note: We do NOT guarantee the quality of items from our $1 sections and therefore do not take returns on these products!**

### How is your record stock organized?

Records are divided into sections based on genre, with our newest stock in the front of the store. Vinyl is sorted alphabetically from left to right. Artists are sorted by their last name, and bands are sorted by the first word in their name. If you are ever looking for something specific and are unsure where to look, feel free to ask the staff and we would be more than happy to assist you in finding that one record you've been searching for!

### Do I need to bring in records to buy records?

Nope! Don’t let our name confuse you, we do standard transactions as well as "swaps."

### What are your shipping policies?

We ship through USPS using Media shipping (4-8 days) or Priority shipping (1-3 days) on request. Domestically, these rates are a flat $4 for Media mail and $10 for Priority class, with a $1 increase per each item added. We do ship internationally! Our prices range for this, so feel free to check out our [shipping policy]({{ site.data.business.links.shipping-policy }}) on Discogs or email us at <a href="mailto:{{ site.data.business.email }}">{{ site.data.business.email }}</a> with specific shipping questions.

### What are your payment options?

Using Paypal Here, we accept almost all major credit/debit cards, including Visa, Discover, Mastercard, and American Express. We also accept PayPal, Apple Pay, Google Pay, and Samsung Pay, as well as various forms of touch-free payment. Other payment options include cash, checks, cashier's checks, money orders, and trade.